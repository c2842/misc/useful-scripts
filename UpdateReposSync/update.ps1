# get teh token
get-content ../.env | ForEach-Object {
  $name, $value = $_.split('=')
  set-content env:\$name $value
}
# headrs are standardised
$headers = @{
  'PRIVATE-TOKEN' = $env:TOKEN
}


function Mirror-Clear {
  param (
    $ID
  )

  $Response = Invoke-RestMethod -URI "https://gitlab.home.brendan.ie/api/v4/projects/$ID/remote_mirrors" -Headers $headers
  $Response | ForEach-Object {
    $ID_Mirror = $_.id
    Invoke-RestMethod -URI "https://gitlab.home.brendan.ie/api/v4/projects/$ID/remote_mirrors/$ID_Mirror" -Headers $headers -Method Delete
  }
}

# curl --request POST --data "url=https://username:token@example.com/gitlab/example.git" \
#     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/42/remote_mirrors"

function Mirror-Create {
  param (
    [Parameter(Mandatory = $true)]
    [int] $ID,
    [Parameter(Mandatory = $true)]
    [string] $REPO,
    [Parameter(Mandatory = $true)]
    [string] $protected,
    [Parameter(Mandatory = $true)]
    [string] $divergent
  )

  $token = $($env:TOKEN_REMOTE)
  if ($REPO.ToLower().Contains("gitlab.skynet.ie")){
    $token = $($env:TOKEN_SKYNET)
    Write-Host $token
  }

  Write-Host $token

  $uri = "https://gitlab.home.brendan.ie/api/v4/projects/$ID/remote_mirrors"
  $Body = @{
    url = $REPO.replace("https://", "https://oauth2:$token@")
    enabled = "true"
    only_protected_branches = $protected
    keep_divergent_refs = $divergent
  }
  Write-Host $Body.url

  Invoke-RestMethod -URI $uri -Headers $headers -Method Post -Body $body

}

function Get-Active-Version {
  $uri = "https://gitlab.home.brendan.ie/college/misc/useful-scripts/-/raw/main/UpdateReposSync/repos.csv";
  Invoke-WebRequest -Uri $uri -OutFile "./repos_active.csv"
}

function Get-Diff-Version {
  # set headers
  Add-Content -Path ./repos_diff.csv -Value "id_local, remote_url"

  $Diff = compare-object (get-content ./repos.csv) (get-content ./repos_active.csv)
  $Diff | ForEach-Object {
    if ($_.SideIndicator -eq "<=") {
      Add-Content -Path ./repos_diff.csv -Value $_.InputObject
    }
  }
}

function Cleanup {
  Remove-Item ./repos_active.csv
  Remove-Item ./repos_diff.csv
}

function Main {
  Get-Active-Version
  Get-Diff-Version

  $Repos = Import-Csv -Path ./repos_diff.csv

  Write-Host "Cleanup"
  $Repos | ForEach-Object {
    Write-Host $_.id_local $_.remote_url

    Mirror-Clear -ID $_.id_local
  }

  Write-Host ""

  Write-Host "Setting"
  $Repos | ForEach-Object {
    Write-Host $_.id_local $_.remote_url

    # currently hardcoded in but could probally do this in teh csv
    Mirror-Create -ID $_.id_local -REPO $_.remote_url -protected "false" -divergent "false"
  }

  Cleanup
}


Main