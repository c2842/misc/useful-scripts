#!/usr/bin/env bash

root="$PWD"
for folder in *; do
    if [ ! -d "$folder" ]; then
        continue;
    fi
    echo "$folder"
    cd "$root/$folder"

    # check if anything needs to be committed
    tmp=$(git status -s)
    
    # if there are changes then commit them
    git add -A
    git commit -sm "doc: updated repo"    

    # pull before push
    git pull origin main
    git push private main

    cd "$root"
done
