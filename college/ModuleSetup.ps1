# ./ModuleSetup.ps1 C:\College semester-6
function Create-Semester {
    param (
        [Parameter(Mandatory = $true)]
        [string] $url_base,
        [Parameter(Mandatory = $true)]
        [string] $token,
        [Parameter(Mandatory = $true)]
        [string] $namespace,
        [Parameter(Mandatory = $true)]
        [string] $semester
    )

    $uri = "$url_base/api/v4/groups/"
    $headers = @{
        'PRIVATE-TOKEN' = $token
    }
    $Body = @{
        path = $semester
        name = ( Get-Culture ).TextInfo.ToTitleCase( $semester.replace('-',' ') )
        parent_id = $namespace
        visibility = "public"
    }

    $response = Invoke-RestMethod -URI $uri -Headers $headers -Method Post -Body $body
    return $response.id
}

function Create-Module {
    param (
        [Parameter(Mandatory = $true)]
        [string] $url_base,
        [Parameter(Mandatory = $true)]
        [string] $token,
        [Parameter(Mandatory = $true)]
        [string] $namespace,
        [Parameter(Mandatory = $true)]
        [string] $module,
        [Parameter(Mandatory = $true)]
        [string] $description
    )

    $uri = "$url_base/api/v4/projects/"
    $headers = @{
        'PRIVATE-TOKEN' = $token
    }
    $Body = @{
        path = $module.ToLower()
        name = $module
        description = $description
        namespace_id = $namespace
        visibility = "private"
    }

    $response = Invoke-RestMethod -URI $uri -Headers $headers -Method Post -Body $body
    return $response.id
}

function Main{
    Write-Output "$semester - Start"

    $modules = @()
    switch ($semester) {
        #"semester-4" { $modules = @('CS4006','CS4076','CS4115','CS4815','MA4413') }
        "semester-6" { $modules = @(
            [PSCustomObject]@{
                "Code"="CS4457"
                "Name"="Project Management and Practice"
            },
            [PSCustomObject]@{
                "Code"="CS4187"
                "Name"="Professional Issues in Computing"
            },
            [PSCustomObject]@{
                "Code"="CS4084"
                "Name"="Mobile Application Development"
            },
            [PSCustomObject]@{
                "Code"="CS4116"
                "Name"="Software Development Project"
            },
            [PSCustomObject]@{
                "Code"="CS4106"
                "Name"="Machine Learning: Methods and Applications"
            }
        ) }
    }

    if ($modules.Length -eq 0) {
        Write-Output "Invalid semester"
        return
    }

    # go to teh root dir
    Set-Location $root

    # gotta make the folder look consistant
    $semesterFolderName = ( Get-Culture ).TextInfo.ToTitleCase( $semester.replace('-','_') )
    New-Item -Path . -Name $semesterFolderName -ItemType "directory"

    # go into that folder
    Set-Location $semesterFolderName

    $url_local  = "https://gitlab.home.brendan.ie"
    $url_remote = "https://gitlab.com"

    # create teh gitlab folders
    $group_id_local  = Create-Semester -semester $semester -url_base $url_local  -namespace "4"        -token $env:TOKEN
    $group_id_remote = Create-Semester -semester $semester -url_base $url_remote -namespace "13837762" -token $env:TOKEN_REMOTE
    # git@gitlab.com:c2842/semester-4/cs4006.git
    # git@gitlab.home.brendan.ie:college/semester-4/cs4006.git

    $modules | Foreach-Object {
        $module = $PSItem
        # create the folder
        New-Item -Path . -Name $module.Code -ItemType "directory"

        # go into it
        Set-Location $module.Code

        # get teh git name
        $gitName = $module.Code.ToLower()

        # setup
        git init

        # create repos
        Create-Module -module $module.Code -description $module.Name -url_base $url_local  -namespace "$group_id_local"  -token $env:TOKEN
        Create-Module -module $module.Code -description $module.Name -url_base $url_remote -namespace "$group_id_remote" -token $env:TOKEN_REMOTE

        # remotes
        git remote add origin "git@gitlab.home.brendan.ie:college/$semester/$gitName.git"
        git remote add public "git@gitlab.com:c2842/$semester/$gitName.git"

        # create template files
        # https://gitlab.com/c2842/misc/module-template/-/archive/main/module-template-main.zip
        Invoke-WebRequest "https://gitlab.com/c2842/misc/module-template/-/archive/main/module-template-main.zip" -OutFile ./tmp.zip
        Expand-Archive -Path ./tmp.zip -DestinationPath .
        Copy-Item -Path "./module-template-main/*" -Destination . -Recurse
        Remove-Item ./tmp.zip
        Remove-Item ./module-template-main/ -Recurse

        # commit template files
        git add -A
        git commit -sm "Initial Commit"

        # update the repos
        git push origin main
        git push public main

        # return back to original folder
        Set-Location ../

        Write-Output " - $($module.Code) - $($module.Name)"
    }

    Set-Location ../

    Write-Output "$semester - Complete"
}

# set teh env vars
get-content ../.env | ForEach-Object {
    $name, $value = $_.split('=')
    set-content env:\$name $value
}

$root = $args[0]
$semester = $args[1]

Main