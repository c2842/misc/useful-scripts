Get-ChildItem -Path ./ -Directory |

Foreach-Object {
  echo $_.FullName
  cd $_.FullName
  git add -A
  git commit -sm "doc: updated materials"
}

Read-Host -Prompt "Press Enter to exit"