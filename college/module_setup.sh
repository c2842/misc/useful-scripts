#!/nix/store/kxkdrxvc3da2dpsgikn8s2ml97h88m46-bash-interactive-5.2-p15/bin/bash


function UpperCaseFirst(){
  local value=${1}
  echo "$(echo "$value" | sed 's/.*/\u&/')"
}

function CleanString(){
    local value=${1}
    echo $(tr -d '\n\t\r ' <<<"$value" )
}

function LowerCase() {
    local value=${1}
    echo "$value" | tr '[:upper:]' '[:lower:]'
}

function CreateSemester(){
  local url_base=${1}
  local token=${2}
  local namespace=${3}
  local semester=${4}

  local uri="$url_base/api/v4/groups/"
  local header="PRIVATE-TOKEN: $token"
  # "semester-6" => "Semester 6"
  local name=$(UpperCaseFirst "$(echo "$semester" | sed "s/-/ /")")

  local body="path=$semester&name=$name&parent_id=$namespace&visibility=public"

  response=$(curl -s -X "POST" "$uri" --header "$header" --data "$body")
  echo $(echo "$response" | jq .id)
}

function CreateModule(){
  local url_base=${1}
  local token=${2}
  local namespace=${3}
  local module=${4}
  local description=${5}

  local uri="$url_base/api/v4/projects/"
  local header="PRIVATE-TOKEN: $token"
  # "CS4457" => "cs4457"
  local path=$(LowerCase "$module")
  local body="path=$path&name=$module&description=$description&namespace_id=$namespace&visibility=private"

  echo "$uri"
  echo "$header"
  echo "$body"

  curl -s -X "POST" "$uri" --header "$header" --data "$body"
}


function Main(){
  source ../.env

  local pwd=$PWD
  local root=$(CleanString ${1})
  local semester=$(CleanString ${2})

  local local_token=$(CleanString "$TOKEN")
  local local_url=$(CleanString "$LOCAL_URL")
  local local_namespace=$(CleanString "$LOCAL_NAMESPACE")

  local remote_token=$(CleanString "$TOKEN_REMOTE")
  local remote_url=$(CleanString "$REMOTE_URL")
  local remote_namespace=$(CleanString "$REMOTE_NAMESPACE")

  # create teh root folder
  local semester_dir="$root/$(UpperCaseFirst "$(echo "$semester" | sed "s/-/_/")")"
  mkdir -p "$semester_dir"
  mkdir -p "$semester_dir/tmp"

  # setup the base config
  git clone https://gitlab.com/c2842/misc/module-template.git "$semester_dir/tmp"
  rm -rf "$semester_dir/tmp/.git/"

  # create teh git repos and gert their ID's
  local group_id_local=$(CreateSemester "$local_url" "$local_token" "$local_namespace" "$semester")
  local group_id_remote=$(CreateSemester "$remote_url" "$remote_token" "$remote_namespace" "$semester")

  git config --global init.defaultBranch "main"

  # loop through the csv
  while IFS="," read -r semester_tmp_tmp code_tmp name_tmp
  do
    # clean vars
    local semester_tmp=$(CleanString "$semester_tmp_tmp")
    local code=$(CleanString "$code_tmp")
    local name=$(CleanString "$name_tmp")

    # if the input array contains the module
    if [[ "$semester" == "$semester_tmp" ]]
    then
      local module_dir="$semester_dir/$code"
      mkdir -p "$module_dir"
      echo "$module_dir"
      cd "$module_dir"

      local counter=0

      # create the repos if the folder was created successfully
      if [[ "$group_id_local" != "null" ]]
      then
        CreateModule "$local_url" "$local_token" "$group_id_local" "$code" "$name"
        counter=$((counter + 1))
      fi

      if [[ "$group_id_remote" != "null" ]]
      then
        CreateModule "$remote_url" "$remote_token" "$group_id_remote" "$code" "$name"
        counter=$((counter + 1))
      fi

      # setup
      git init
      # remotes
      git remote add origin "git@gitlab.home.brendan.ie:college/$semester/$(LowerCase "$code").git"
      git remote add private "git@gitlab.com:c2842/$semester/$(LowerCase "$code").git"

      # update the repos
      git pull origin main
      git pull private main

      if [[ "$counter" == "2" ]]
      then
        # copy in the template files
        cp -R "$semester_dir/tmp/." .
        git add -A
        git commit -sm "Initial Commit"
      fi

      # update the repos
      git push origin main
      git push private main

    fi

  done < <(tail -n +2 ./modules.csv)

  # go back to where it was called from
  cd $pwd
  rm -rf "$semester_dir/tmp"
}

Main ${1} ${2}
