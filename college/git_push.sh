#!/usr/bin/env bash

root="$PWD"
for folder in *; do
    if [ ! -d "$folder" ]; then
        continue;
    fi
    echo "$folder"
    cd "$root/$folder"

    # make sure all lfs files make it up to each repo
    git lfs push --all private
    git lfs push --all origin

    git push private main

    cd "$root"
done
