#!/usr/bin/env bash

root="$PWD"
for folder in *; do
    if [ ! -d "$folder" ]; then
        continue;
    fi
    echo "$folder"
    cd "$root/$folder"

    git status
    
    cd "$root"
done