#!/usr/bin/env bash

root="$PWD"

message="$1"
if [ -z "${message}" ]; then
  message="doc: updated repo"
fi

for folder in *; do
    if [ ! -d "$folder" ]; then
        continue;
    fi
    echo "$folder"
    cd "$root/$folder"

    git add -A
    git commit -sm "$message"

    cd "$root"
done