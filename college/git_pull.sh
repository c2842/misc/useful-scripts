#!/usr/bin/env bash

root="$PWD"
for folder in *; do
    if [ ! -d "$folder" ]; then
        continue;
    fi
    echo "$folder"
    cd "$root/$folder"

    git lfs fetch --all origin
    git fetch origin
    git pull origin main
    # dont pull origin,
    git fetch private main

    cd "$root"
done
